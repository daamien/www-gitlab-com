---
layout: markdown_page
title: "Online Marketing"
---
Welcome to the Online Marketing Handbook.

## On this page

* [URL Tagging](#urlTagging)
* [Weekly Website Health Check](#healthCheck)

## URL Tagging<a name="urlTagging"></a>

All marketing campaigns that are promoted on external sites and through email should use URL tagging to increase the data cleanliness in Google Analytics. For a primer course, please see the [training video](https://drive.google.com/a/gitlab.com/file/d/0B1_ZzeTfG3XYNWVqOC11NWpKWjA/view?usp=sharing). This explains the why and how of URL tagging.

Our internal URL tagging tool can be accessed on Google Docs under the name [Google Analytics Campaign Tagging Tool](https://docs.google.com/a/gitlab.com/spreadsheets/d/12jm8q13e3-JNDbJ5-DBJbSAGprLamrilWIBka875gDI/edit?usp=sharing).

## Weekly Website Health Check<a name="healthCheck"></a>

Every Friday a website health check should be performed. This check is meant to ensure that there are no issues with the website that could cause issues with traffic to the site. These are the following things to check to ensure the health of the site:

* [Google Webmaster Tools](https://www.google.com/webmasters/tools/)
* [Bing Webmaster Tools](http://www.bing.com/toolbox/webmaster)
* [Google Analytics](https://analytics.google.com/analytics/web/)

Issues to look for in each of these tools:

* **Google Webmaster Tools**: Check the dashboard and messages for any important notifications regarding the website. Also check under `Search Traffic` > `Manual Actions` for any URLs that have been identified as spam or harmful content.
* **Bing Webmaster Tools**: Check all the links under `Security`.
* **Google Analytics**: Compare organic site traffic from the most previous week compared to the previous week and look for any large fluctuations.
